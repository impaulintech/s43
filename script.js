// Balance
let totalBalance = 230.75
let loadBalance = document.querySelector("#balance");
loadBalance.innerHTML = totalBalance;

// Number
let number = document.getElementsByClassName("form-control");
//target => number[0].value

// transaction
let history = document.getElementById("transaction")
history.innerHTML = ""; //insert new transaction

let ten = document.getElementById("ten");
let fifty = document.getElementById("fifty");
let oneH = document.getElementById("oneH");

function loadingProcess(load) {
    //Number Validator
    if (number[0].value.length < 10) {
        return alert("Please enter valid mobile number");
    }

    //Balance Checker
    if (loadBalance.innerHTML < load) {
        return alert("Insufficient Balance");
    }

    //deduct load + tax
    loadBalance.innerHTML -= load

    //Remove tax and save in transaction history
    let removeTax = () => {
        // Load + Tax
        // 10 = 3.25
        // 50 = 2.50
        // 100 = 2.50
        if (load == 13.50) { load = 10 }
        if (load == 52.25) { load = 50 }
        if (load == 102.25) { load = 100 }
    }

    //Push to transaction history logs
    removeTax();
    history.innerHTML += `&#8369;${load}.00 is loaded to 0${number[0].value}\n`

}

function load() {

    //Load 10
    ten.addEventListener("click", () => {

        //Run loading process
        loadingProcess(13.50);

    })

    //Load 50
    fifty.addEventListener("click", () => {

        //Run loading process
        loadingProcess(52.25);

    })

    //Load 100
    oneH.addEventListener("click", () => {

        //Run loading process
        loadingProcess(102.25);

    })

}

load(); 